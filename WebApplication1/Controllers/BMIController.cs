﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.ViewModel;

namespace WebApplication1.Controllers
{
    public class BMIController : Controller
    {
        // GET: BMI
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(BMIData data)
        {
            if (data.Height < 50 || data.Height > 200)
            {
                ViewBag.Herror = "50~200";
            }

            if (data.Weight < 30 || data.Weight > 300)
            {
                ViewBag.Werror = "30~300";
            }
            float m_height = data.Height / 100;
            float bmi = data.Weight / (m_height * m_height);

            string level = "";
            if (bmi < 18.5)
            {
                level = "太瘦";
            }
            else if (bmi > 18.5 && bmi < 24)
            {
                level = "適中";
            }
            else if (bmi > 35)
            {
                level = "太胖";
            }
            data.BMI = bmi;
            data.Level = level;



            return View(data);
        }

    }
}